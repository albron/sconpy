# -*- coding: utf-8 -*-

import numpy as np
from sconpy import D0
from sconpy.transport import SIS

from matplotlib import pylab as plt

Tc1 = 3.5
Tc2 = 1.2

Delta1 = D0*Tc1
Delta2 = D0*Tc2
Delta = Delta1 + Delta2
    
T1 = Tc1/4.
T2 = Tc1/4.
dynes = np.sqrt(1e-5)

Vbias = np.linspace(0., 1.3, 1001)*(Delta)

I = SIS(Vbias, 1., T1, T2, Tc1, Tc2, dynes, dynes, False)
Q = SIS(Vbias, 1., T1, T2, Tc1, Tc2, dynes, dynes, True)

plt.figure(figsize=(8,3))

plt.subplot(121)
plt.plot(Vbias/Delta, I/Delta1, label=r'$T_1$, $T_2$ = $T_{c1}/4$')
plt.xlabel(r'bias voltage ($(\Delta_1+\Delta_2)/e$)')
plt.ylabel(r'quasiparticle current ($G_T \Delta_1 / e$)')
plt.legend(loc='upper left')

plt.subplot(122)
Ps = (Vbias*I)/Delta1/Delta1

plt.plot(Vbias/Delta, Q/Delta1/Delta1, label=r'S2-side')
plt.plot(Vbias/Delta, Ps, ':', label=r'S1-side')

plt.fill_between(Vbias/Delta, 0, Q/Delta1/Delta1, where=Q<0., alpha=0.3)
plt.fill_between(Vbias/Delta, 0, Q/Delta1/Delta1, where=Q>0., alpha=0.3)

plt.ylim([-0.05, 0.2])

plt.xlabel(r'bias voltage ($(\Delta_1+\Delta_2)/e$)')
plt.ylabel(r'power [$G_T (\Delta_1 / e)^2$]')
plt.legend(loc='upper left')
    
plt.tight_layout()
plt.savefig('SIS_cooler.pdf')
