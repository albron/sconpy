# -*- coding: utf-8 -*-

import numpy as np
from sconpy import D0
from sconpy.transport import NIS, V_NIS

from matplotlib import pylab as plt

Tc = 1.2
Delta = D0*Tc
    
TS = Tc/4.
TN = Tc/4.
dynes = 1e-5

Ibias = np.linspace(0., 3.5, 201)*Delta
Vbias = np.linspace(0., 2.2, 201)*Delta

V = V_NIS(Ibias, 1., TN, TS, Tc, dynes)
I = NIS(Vbias, 1., TN, TS, Tc, dynes, False)
Q = NIS(Vbias, 1., TN, TS, Tc, dynes, True)
Qi = NIS(V, 1., TN, TS, Tc, dynes, True)

plt.figure(figsize=(8,3))

plt.subplot(121)
plt.plot(Vbias/Delta, I/Delta, label=r'$T_n$, $T_s$ = $T_c/4$')
plt.xlabel(r'bias voltage ($\Delta_0/e$)')
plt.ylabel(r'quasiparticle current ($G_T \Delta_0 / e$)')
plt.legend(loc='upper left')

plt.subplot(122)
Ps = (Vbias*I)/Delta/Delta

plt.plot(Vbias/Delta, Q/Delta/Delta, label=r'n-side')
plt.plot(Vbias/Delta, Ps, ':', label=r's-side')

plt.fill_between(Vbias/Delta, 0, Q/Delta/Delta, where=Q<0., alpha=0.3)
plt.fill_between(Vbias/Delta, 0, Q/Delta/Delta, where=Q>0., alpha=0.3)

plt.xlim([0., 1.35])
plt.ylim([-0.05, 0.2])

plt.xlabel(r'bias voltage ($\Delta_0/e$)')
plt.ylabel(r'power [$G_T (\Delta_0 / e)^2$]')
plt.legend(loc='upper left')
    
plt.tight_layout()
plt.savefig('NIS_cooler.pdf')
