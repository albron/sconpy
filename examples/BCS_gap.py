import numpy as np
from matplotlib import pylab as plt

from sconpy import D0
from sconpy.BCS import gap, DOS, peak_sing


T = np.linspace(1e-3, 1.2, 1000)
g = gap(1., T)/D0
dynes = 1e-3


plt.figure(figsize=(8,3))
plt.subplot(121)
plt.plot(T, g, label='analyt. approx.')
plt.xlabel('T / Tc')
plt.ylabel('Delta / Delta(0)')
plt.legend()

plt.subplot(122)
for t in [0.01, 0.5, 0.7, 0.8, 0.9, 0.99]:
    gt = gap(1., t)/D0
    
    e0 = peak_sing(dynes)

    E0 = np.hstack([
        1 - np.logspace(-5, 0, 61),
        1 + np.logspace(-5, 0, 61),])*e0

    E = np.hstack([ E0, np.linspace(E0[-1], 5/gt, 100),
                   -E0, -np.linspace(E0[-1], 5/gt, 100)])

    x = np.unique(E)
    y = DOS(x, dynes)
    plt.semilogy(x*gt, y, label='T/Tc = {:.2f}'.format(t ))

plt.xlabel('E / Delta(0)')
plt.ylabel('DOS(E)')
plt.xlim(-2, 5)

plt.legend()




plt.tight_layout()
plt.savefig('BCS_gap.pdf')
