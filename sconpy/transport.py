# -*- coding: utf-8 -*-
import numpy as np

from scipy.constants import k, h
from scipy.constants import elementary_charge as q
from scipy.integrate import quad
from scipy import LowLevelCallable
from scipy.optimize import root_scalar, minimize_scalar

# numba accelerated
from numba import jit, cfunc, carray, vectorize
from numba.types import float64, intc, CPointer, boolean, float32

from sconpy import D0, L0
from sconpy.BCS import _gap_approx


@cfunc(float64(intc, CPointer(float64)))
def _int_gthm(n, v_ptr):
    x, = carray(v_ptr, (n,), dtype=float64)
    return np.square(x/np.cosh(x*0.5))


def _gthm_approx(T, Tc, cutoff = 1e-4):
    d = np.pi*np.exp(-np.euler_gamma)*Tc/T
    
    p = [-1.66365129e-19, 2.14808320e-16, -1.15193318e-13, 3.35085735e-11,
         -5.78416160e-09, 6.07788016e-07, -3.82371606e-05, 1.35030903e-03,
          5.85141047e-01,  1.31392719e+00,  2.00292749e+00]
    
    ka= np.polyval(p, d)*np.exp(-d)
    
    return np.hypot(ka, cutoff)


@np.vectorize
def gthm(T, Tc=0., cutoff=1e-4):
    y = _gap_approx(Tc/T)*Tc/T*np.pi/np.exp(np.euler_gamma)
    if Tc==0.:
        return 1.
    NMAX = 376
    if y >= NMAX:
        return cutoff
    
    integrand = LowLevelCallable(_int_gthm.ctypes)
    
    res = quad(integrand, y, NMAX,
           epsabs = 0, epsrel=2e-14, full_output=0)
    
    return np.hypot(res[0] * 3.0/(2*np.pi*np.pi), cutoff)
    

@cfunc(float64(intc, CPointer(float64)))
def _int_SIS(n, v_ptr):
    x, v, b1, b2, d1, d2, dyn1, dyn2, qexp = carray(v_ptr, (n,), dtype=float64)

    fd = np.reciprocal(1+np.exp(b2*(x-v)))-np.reciprocal(1+np.exp(b1*x))

    if d1 > 0.:
        cx1 = x/d1+1j*dyn1
        dos1 = np.absolute(np.real(cx1/np.sqrt(cx1*cx1-1)))
    else:
        dos1 = 1.

    if d2 > 0.:
        cx2 = (x-v)/d2+1j*dyn2
        dos2 = np.absolute(np.real(cx2/np.sqrt(cx2*cx2-1)))
    else:
        dos2 = 1.

    return fd*dos1*dos2*np.power(v-x, qexp)


def _quad_SIS(v, beta1, beta2, d1, d2, dynes1, dynes2, qdot):

    if v >= 0.:
        a = -35.0 / beta1
        b = v+ 35.0 / beta2
    else:
        a = v-35.0 / beta2
        b = 35 / beta1

    if not qdot:
        res = quad(LowLevelCallable(_int_SIS.ctypes), a, b, args=(v, beta1, beta2, d1, d2, dynes1, dynes2, 0.0),
                 limit=1000, epsabs = 0, epsrel=1e-9, full_output=1)
    else:
        res = quad(LowLevelCallable(_int_SIS.ctypes), a, b, args=(v, beta1, beta2, d1, d2, dynes1, dynes2, 1.0),
                 limit=1000, epsabs = 0, epsrel=1e-9, full_output=1)
    return res[0]


@np.vectorize
def SIS(vbias, RT, T1, T2, Tc1, Tc2, dynes1, dynes2, qdot=False): 
    
    d1 = _gap_approx(Tc1/T1)*Tc1
    d2 = _gap_approx(Tc2/T2)*Tc2

    beta1 = q*D0 / (k*T1)
    beta2 = q*D0 / (k*T2)
    i = _quad_SIS(vbias/D0, beta1, beta2, d1, d2, dynes1, dynes2, qdot)

    if not qdot:
        return i*D0/RT
    else:
        return i*D0*D0/RT


@np.vectorize
def NIS(vbias, RT, TN, TS, Tc, dynes, qdot=False): 
    return SIS(vbias, RT, TS, TN, Tc, 0., dynes, dynes, qdot) 


@np.vectorize
def V_NIS(ibias, RT, TN, TS, Tc, dynes):

    func = lambda x: ibias-NIS(x, RT, TN, TS, Tc, dynes, False)
    if ibias == 0.:
        return 0.
    
    vmin = ibias*RT/dynes
    sol = root_scalar(func, bracket=[0, vmin])
    
    return sol.root
