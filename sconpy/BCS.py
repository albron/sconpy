# -*- coding: utf-8 -*-

import numpy as np

from scipy.constants import elementary_charge as q
from scipy.optimize import fminbound

from sconpy import D0

from numba import vectorize

@vectorize
def _gap_approx(t):
    # this analytical approximation should be valid within 1%
    # FIXME: add reference
    if t <= 1.:
        return 0.
    else:
        return np.tanh(1.82*np.power(1.018*(t-1), 0.51))


def gap(Tc=1., T=1e-4, approx=True, units='eV'):
        
    if units == 'SI':
        E0 = D0*q
    else:
        E0 = D0
    
    if approx:
        # analytical expression, fast computing
        d = _gap_approx(Tc/T)
    else:
        # FIXME: calculate through proper BCS integral
        #        find a way to calculate once and cache results
        raise NotImplementedError
    
    return E0*Tc*d


def peak_sing(dynes, refine=True):
    x0 = 1.+8.065E-8+0.5862*dynes # rough initial estimate
    if refine:
        func = lambda x: -DOS(x, dynes)
        x0 = fminbound(func, x0-10*dynes, x0+10*dynes, xtol = np.square(dynes)*1e-2)
    return x0


def DOS(E, dynes=1e-5):
    z = E + 1j*dynes
    cz = np.nan_to_num(np.real(z/np.sqrt(np.square(z)-1)))
    return np.absolute(cz)
    
