import numpy as np
from scipy.constants import k, h
from scipy.constants import elementary_charge as q

# units: eV / K
# BCS gap constant ~ 1.76
D0 = np.pi/np.exp(np.euler_gamma)*k/q

# Lorenz factor
L0 = np.square(k*np.pi/q)/3.0
