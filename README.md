# sconpy

Numeric tools for mesoscopic superconductivity

## install

  * git clone https://gitlab.com/albron/sconpy.git
  * cd sconpy
  * pip install --user .

## usage

See examples directory


